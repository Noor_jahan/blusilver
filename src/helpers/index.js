export const formatTimestamp = (timestamp)=>{
    let formattedDate = ''
    if(timestamp !== ''){
        const months = ['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
        const date = new Date(timestamp)
        let ampm = date.getHours() >= '12' ? 'PM' : 'AM'
        const formattedDate = date.getDate() + ' ' + months[parseInt(date.getMonth())] + ' ' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ' ' + ampm
        return formattedDate    
    }else{
        return formattedDate
    }
}

export const formatTimestampForCharts = (timestamp)=>{
    let formattedDate = ''
    if(timestamp !== ''){
        const months = ['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
        const date = new Date(timestamp)
        // let ampm = date.getHours() >= '12' ? 'PM' : 'AM'
        // const formattedDate = date.getDate() + ' ' + months[parseInt(date.getMonth()+1)] + ' ' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes() + ' ' + ampm
        const formattedDate = date.getDate() + ' ' + months[parseInt(date.getMonth())] + ' ' + date.getFullYear()
        return formattedDate    
    }else{
        return formattedDate
    }
}

export const formatTimestampForDob = (timestamp)=>{
    let formattedDate = ''
    if(timestamp !== ''){
        const months = ['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
        const date = new Date(timestamp)
        const formattedDate = date.getDate() + ' ' + months[parseInt(date.getMonth())] + ' ' + date.getFullYear()
        return formattedDate    
    }else{
        return formattedDate
    }
}

export const formatTimestampForSettingsPage = (timestamp)=>{
    let formattedDate = ''
    if(timestamp !== ''){
        const months = ['Jan','Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov','Dec']
        const date = new Date(timestamp)
        const formattedDate = months[parseInt(date.getMonth()+1)] + '-' + date.getDate()+ '-' + date.getFullYear()
        return formattedDate    
    }else{
        return formattedDate
    }
}