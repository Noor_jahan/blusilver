import Footer from "./components/Footer";
import Header from "./components/Header";
import Login from "./components/Login";
import { Switch , Route } from 'react-router';
import Otp from './components/Otp';
import Home from "./components/Home";
import Alerts from './components/Alerts';
import Settings from './components/Settings';
import InmateForm from './components/InmateForm';
import './font.css';
import HeartChart from "./components/HeartChart";
import RestlessnessChart from "./components/RestlessnessChart";
import StressChart from "./components/StressChart";
import Management from "./components/Management";
import ChartSample from "./components/ChartSample";
import {useEffect,useState,useCallback} from 'react';
import ChartComponent from './components/ChartCompopnent/ChartComponent'
function App() {
  return (
    <Switch>
      <Route exact path="/">
        <Header activeNav="management" name="Kumar Ullas" logo="BluCare Home"/>
        <Management/>
        <Footer/>
      </Route>
      <Route path="/admin">
        <Header activeNav="home" name="Kumar Ullas" logo="BluCare Home" />
        <Home/>
        <Footer/>
      </Route>
      <Route path="/login">
        <Header addClass="login__header" logo="BluGraph"/>
        <Login/>
      </Route>
      <Route path="/otp-verify">
        <Header addClass="login__header" logo="BluGraph"/>
        <Otp/>
      </Route>
      <Route path="/alerts">
        <Header activeNav="alerts" name="Kumar Ullas" logo="BluCare Home"/>
        <Alerts/>
        <Footer/>
      </Route>
      <Route exact path="/settings">
        <Header activeNav="settings" name="Kumar Ullas" logo="BluCare Home"/>
        <Settings/>
        <Footer/>
      </Route>
      <Route exact path="/settings/inmate/create">
        <Header activeNav="settings" name="Kumar Ullas" logo="BluCare Home"/>
        <InmateForm action="add" />
        <Footer/>
      </Route>
      <Route path="/settings/inmate/edit/:id">
        <Header activeNav="settings" name="Kumar Ullas" logo="BluCare Home"/>
        <InmateForm action="edit" />
        <Footer/>
      </Route>
      <Route exact path="/bar">
        <ChartSample/>
      </Route>
      <Route path="/chart/heartrate/:id/:name">
        <Header activeNav="alerts" name="Kumar Ullas" logo="BluCare Home"/>
        {/*<HeartChart/>*/}
        <ChartComponent />
        <Footer/>
      </Route>
      <Route path="/chart/restlessness/:id/:name">
        <Header activeNav="alerts" name="Kumar Ullas" logo="BluCare Home"/>
        {/*<RestlessnessChart/>*/}
        <ChartComponent />
        <Footer/>
      </Route>
      <Route path="/chart/stress/:id/:name">
        <Header activeNav="alerts" name="Kumar Ullas" logo="BluCare Home"/>
        {/*<StressChart/>*/}
        <ChartComponent />
        <Footer/>
      </Route>
    </Switch>
  );
}

export default App;