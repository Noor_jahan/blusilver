import axios from 'axios'

export default axios.create({
    baseURL:"https://blusilver-api.blugraph.services/blusilver/",
    // baseURL:"",
    responseType:"json",
    headers: {
        "Content-Type": "application/json",
        "Accept":'application/json'
    }
})

export const cancelToken = axios.CancelToken