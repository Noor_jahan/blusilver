import React, { useState, useEffect} from 'react'
import './css/table.css'
import Table from '@material-ui/core/Table'
import { TableContainer, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import {useHistory} from 'react-router-dom'
import { formatTimestamp , formatTimestampForDob, formatTimestampForSettingsPage } from '../helpers'

function PatientsTable({rows}) {

    const history = useHistory()
    
    const [mainRows,setMainRows] = useState(rows)

    console.log(rows)

    const showEditForm = (id)=>{
        if(!id){
            alert('Error getting inmate id')
            return false
        }
        history.push(`/settings/inmate/edit/${id}`)
    }

    const releaseInmate = async (id) =>{
        if(!id){
            alert('Error getting inmate id')
            return false
        }
        try{
            let data = {
                inmate_id:id
            }
            const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/release',{
                method:'POST',
                headers:{
                    'Accept':'Application/json',
                    'Content-Type':'Application/json'
                },
                body:JSON.stringify(data)
            })

            const content = await response.json()
            if(response.status === 200){
                alert(content.message)
                let newRows = rows.filter((item)=>item.inmate_id !== id)
                setMainRows(newRows)
            }
        }catch(err){
            console.log(err.message)
        }
    }

    useEffect(()=>{
        setMainRows(rows)
    },[rows])
    
    return (
        <div>
            <TableContainer className="table__container">
                <Table aria-label="patients table" className="table">
                    <TableHead className="table__header">
                        <TableRow>
                            <TableCell>
                                Sr.No.
                            </TableCell>
                            <TableCell>
                                Name
                            </TableCell>
                            <TableCell>
                                Email id
                            </TableCell>
                            <TableCell>
                                Phone no.
                            </TableCell>
                            <TableCell>
                                D.O.B
                            </TableCell>
                            <TableCell>
                                Gender
                            </TableCell>
                            <TableCell>
                                Entry Date
                            </TableCell>
                            <TableCell>
                                Exit Date
                            </TableCell>
                            <TableCell>
                                Bed No.
                            </TableCell>
                            <TableCell>
                                Action
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className="table__body">
                        {
                            mainRows.length <= 0
                            ?
                            <TableRow>
                                <TableCell colSpan="4">
                                    No inmates found
                                </TableCell>
                            </TableRow>
                            :
                            null
                        }
                        {
                            mainRows.map((item,index)=>{
                                return (
                                    <TableRow key={index}>
                                        <TableCell>
                                            {item?.sr_no}
                                        </TableCell>
                                        <TableCell>
                                            {item?.inmate_name}   
                                        </TableCell>
                                        <TableCell>
                                            {item?.email_id}
                                        </TableCell>
                                        <TableCell>
                                            {item?.phone_no}
                                        </TableCell>
                                        <TableCell align="center">
                                            {formatTimestampForSettingsPage(item?.DOB)}
                                        </TableCell>
                                        <TableCell align="center">
                                            {item?.gender}
                                        </TableCell>
                                        <TableCell align="center">
                                            {formatTimestampForSettingsPage(item?.entry_date)}
                                        </TableCell>
                                        <TableCell className={item?.exit_date ? '' : 'dash'} align="center">
                                            {item?.exit_date ? formatTimestampForSettingsPage(item?.exit_date) : '-'}
                                        </TableCell>
                                        <TableCell align="center">
                                            {item?.bed_no}
                                        </TableCell>
                                        <TableCell>
                                            <EditIcon style={{ "color":"var(--black)","fontSize":"2rem","marginRight":"0.5rem","cursor":"pointer" }} onClick={()=>showEditForm(item?.inmate_id)} />
                                            <DeleteIcon style={{ "color":"var(--black)","fontSize":"2rem","cursor":"pointer" }} onClick={()=>releaseInmate(item?.inmate_id)}  />
                                        </TableCell>
                                    </TableRow>
                                )
                            })
                        }
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

export default PatientsTable
