import React from 'react'

function RectangleSvg({x,y,barWidth,barHeight,barColor}) {
    return (
        <rect x={x} y={y} width={barWidth} height={barHeight} fill={barColor}></rect> 
    )
}

export default RectangleSvg
