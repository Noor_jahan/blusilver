import React, { useState ,useEffect, useRef } from 'react'
import './css/inmateform.css'
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import { useParams } from 'react-router'
import {Link} from 'react-router-dom'
import axios,{cancelToken} from '../axios'
import { useHistory } from 'react-router'
import phoneUtil from 'google-libphonenumber'
const CssTextField = withStyles({
    root: {
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'var(--mdcBorderColor)',
                borderWidth: '2px',
            },
            '& .MuiSelect-outlined':{
                borderColor: 'var(--mdcBorderColor)',
            },
            '&:hover fieldset': {
                borderColor: 'var(--mdcBorderColor)',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'var(--mdcBorderColor)',
            },
        },
        '& label.Mui-focused': {
            color: 'var(--blue)',
        },
    },
})(TextField);

function InmateForm({action}) {

    const history = useHistory()

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [mobile, setMobile] = useState('')
    const [dob, setDob] = useState('')
    const [gender, setGender] = useState('')
    const [bedNumber, setBedNumber] = useState('')
    const [buildingNumber, setBuildingNumber] = useState('')
    const [wingNumber, setWingNumber] = useState('')
    const [floorNumber, setFloorNumber] = useState('')
    const [entryDate, setEntryDate] = useState('')

    const [buildings,setBuildings] = useState([])
    const [wings,setWings] = useState([])
    const [floors,setFloors] = useState([])
    const [beds,setBeds] = useState([])

    const filterRef1 = useRef(null)
    const filterRef2 = useRef(null)
    const filterRef3 = useRef(null)
    const filterRef4 = useRef(null)

    const params = useParams()

    const inmateId = params.id

    const handleSubmit = (e)=>{
        e.preventDefault()
        if(gender === ''){
            alert('Please select gender')
            return 0
        }
        const number = phoneUtil.PhoneNumberUtil.getInstance();
        const phone = number.parse(mobile);

        if(!number.isValidNumber(phone)){
            alert('Please Enter a valid Phone number with Country code');
            return 0
        }

        action === 'add' 
        ?
            addInmate()
        :
            editInmate()        
    }

    const addInmate = async ()=>{
        console.log(action,name,email,mobile,dob,gender,bedNumber,buildingNumber,wingNumber,floorNumber,bedNumber,entryDate)        

            let data = {
                "inmate_name":name,
                "email_id":email,
                "phone_no":mobile,
                "DOB":dob,
                "gender":gender,
                "building_id":buildingNumber,
                "wing_id":wingNumber,
                "floor_id":floorNumber,
                "bed_id":bedNumber,
                "entry_date":entryDate
            }

            try{
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/add', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });

                const content = await response.json();

                if(response.status === 200){
                    if(content.inmate_id){
                        alert(content.message)
                        history.push('/settings')
                    }
                }else{
                    alert('Error posting data')
                }

            }catch(err){
                console.log(err.message)
            }
        }

    const editInmate = async ()=>{
        console.log(action,name,email,mobile,dob,gender,bedNumber,buildingNumber,wingNumber,floorNumber,bedNumber,entryDate)        

        let data = {
            "inmate_id":inmateId,
            "inmate_name":name,
            "email_id":email,
            "phone_no":mobile,
            "DOB":dob,
            "gender":gender,
            "building_id":buildingNumber,
            "wing_id":wingNumber,
            "floor_id":floorNumber,
            "bed_id":bedNumber,
            "entry_date":entryDate
        }

        try{
            const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/edit', {
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });

            const content = await response.json();

            if(response.status === 200){
                if(content.inmate_id){
                    alert(content.message)
                    history.push('/settings')
                }
            }else{
                alert('Error posting data')
            }

        }catch(err){
            console.log(err.message)
        }       
    }

    const setContent = (data)=>{
        setName(data.inmate_name)
        setEmail(data.email_id)
        setMobile(data.phone_no)
        setDob(data.DOB)
        setGender(data.gender)
        setBedNumber(data.bed_id)
        setBuildingNumber(data.building_id)
        setWingNumber(data.wing_id)
        setFloorNumber(data.floor_id)
        let date = new Date(data.entry_date) 
        let entry_date_year = date.getFullYear()
        let entry_date_month = (date.getMonth()) < 10 ? '0'+parseInt(date.getMonth()+1) :parseInt(date.getMonth()+1) 
        let entry_date_date = (date.getDate()) < 10 ? '0'+parseInt(date.getDate()) :parseInt(date.getDate()) 
        setEntryDate(entry_date_year+'-'+entry_date_month+'-'+entry_date_date)
    }

    useEffect(()=>{
        if(gender === ''){
            setGender('Male')
        }
        const controller = new AbortController()
        const signal = controller.signal
        let isCancelled = false

        const fetchData = async ()=>{
            let data = {
                inmate_id:inmateId
            }
            try{
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/details/list/single', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });

                const content = await response.json();

                if(response.status === 200 && !isCancelled){
                    setContent(content.inmate_details[0])
                    let building_id = (content?.inmate_details[0]?.building_id)
                    let wing_id = (content?.inmate_details[0]?.wing_id)
                    let floor_id = (content?.inmate_details[0]?.floor_id)
                    let bed_id = (content?.inmate_details[0]?.bed_id)
                    
                    const response = await axios.get('/building_structure',{
                        cancelToken: source.token
                    })
                    if(response.status === 200){
                        const buildingSelected = response.data.building_structure.find((item)=>{
                            return item.building_id === building_id
                        })
                        const wingsInBuilding = buildingSelected.wing_details
                        const array = []
                        wingsInBuilding.map((item)=>{
                            return array.push({
                                id:item.wing_id,
                                name:item.wing_name,
                                floors:item.floor_details
                            })
                        })
                        setWings(array)
                        const wingSelected = buildingSelected?.wing_details?.find((item)=>{
                            return item.wing_id === wing_id
                        })
                        const floorsInWing = wingSelected?.floor_details
                        const array2 = []
                        floorsInWing.map((item)=>{
                            return array2.push({
                                id:item.floor_id,
                                name:item.floor_name,
                            })
                        })
                        setFloors(array2)
                        const fetchData = async (paramData)=>{
                            try{
                                let data = paramData
                                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/bed/status', {
                                    method: 'POST',
                                    headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify(data)
                                });
                                const content = await response.json();
                
                                if(response.status === 200){
                                    const unassignedBeds = content?.bed_status.map((item)=>{
                                        return item?.beds.filter((i)=>{
                                            return i?.occupancy_status === "unassigned" || i?.bed_id === bed_id
                                        })
                                    })
                                    if(unassignedBeds[0]?.length === 0){
                                        alert('All beds are occupied')
                                    }else{
                                        let array = []
                                        unassignedBeds[0]?.map((item)=>{
                                            let bed = {
                                                id:item?.bed_id,
                                                name:"Bed " + item?.bed_id.slice(-2)
                                            }
                                            return array.push(bed)
                                        })
                                        setBeds(array)
                                    }
                                }else{
                                    console.log("a")
                                    alert('Error while fetching data')
                                }
                            }catch(err){
                                console.log(err.message)
                            }
                        }
                        let data = {
                            building_id:building_id,
                            wing_id:wing_id,
                            floor_id:floor_id
                        }
                        fetchData(data)
                    }

                }else{
                    console.log("b")
                    alert('Error fetching data')
                }

            }catch(err){
                console.log(err.message)
            }
        }
        const source = cancelToken.source()
        const fetchData2 = async ()=>{
            try{
                    const response = await axios.get('building_structure',{
                    cancelToken: source.token
                })
                if(response.status === 200 && !isCancelled){
                    const array = []
                    response.data.building_structure.filter((item)=>{
                        return array.push({
                            id:item.building_id,
                            name:item.building_name,
                            wings:item.wing_details,
                        })
                    })
                    setBuildings(array)
                }else{
                    alert('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        if(inmateId){
            fetchData()
        }
        fetchData2()

        return(()=>{
            setGender('')
            isCancelled = true
            controller.abort()
            console.log('Abort while unmounting')
        })
    },[])

    const handleFilterChange = (e)=>{
        switch(e.target.name){
            case 'buildingNumber':
                if(e.target.value !== ''){
                    setBuildingNumber(e.target.value)
                    setWings([])
                    setFloors([])
                    filterRef2.current.value=''
                    filterRef3.current.value=''
                    const buildingSelected = buildings.find((item)=>{
                        return item.id === e.target.value
                    })
                    const wingsInBuilding = buildingSelected.wings
                    const array = []
                    wingsInBuilding.map((item)=>{
                        return array.push({
                            id:item.wing_id,
                            name:item.wing_name,
                            floors:item.floor_details
                        })
                    })
                    setWings(array)
                }
                break;
            case 'wingNumber':
                if(e.target.value !== ''){
                    setWingNumber(e.target.value)
                    setFloors([])
                    const wingSelected = wings.find((item)=>{
                        return item.id === e.target.value
                    })
                    filterRef3.current.value=''
                    const floorsInWing = wingSelected.floors
                    const array = []
                    floorsInWing.map((item)=>{
                        return array.push({
                            id:item.floor_id,
                            name:item.floor_name,
                        })
                    })
                    setFloors(array)
                }
                break;
            case 'floorNumber':
                if(e.target.value !== ''){
                    setFloorNumber(e.target.value)
                    const fetchData = async (paramData)=>{
                        try{
                            let data = paramData
                            const response = await fetch('https://blusilver-api.blugraph.services/blusilver/bed/status', {
                                method: 'POST',
                                headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(data)
                            });
                            const content = await response.json();
            
                            if(response.status === 200){
                                const unassignedBeds = content?.bed_status.map((item)=>{
                                    return item?.beds.filter((i)=>{
                                        return i?.occupancy_status === "unassigned"
                                    })
                                })
                                if(unassignedBeds[0]?.length === 0){
                                    setBeds([])
                                    alert('All beds are occupied')
                                }else{
                                    let array = []
                                    unassignedBeds[0]?.map((item)=>{
                                        let bed = {
                                            id:item?.bed_id,
                                            name:"Bed " + item?.bed_id.slice(-2)
                                        }
                                        return array.push(bed)
                                    })
                                    setBeds(array)
                                }
                            }else{
                                alert('Error while fetching data')
                            }
                        }catch(err){
                            console.log(err.message)
                        }
                    }
                    let data = {
                        building_id:buildingNumber,
                        wing_id:wingNumber,
                        floor_id:e.target.value
                    }
                    fetchData(data)
                }
                break;
            default:
                console.log("Select not found")
        }
    }

    const releaseInmate = async (id) =>{
        if(!id){
            alert('Error getting inmate id')
            return false
        }
        try{
            let data = {
                inmate_id:id
            }
            const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/release',{
                method:'POST',
                headers:{
                    'Accept':'Application/json',
                    'Content-Type':'Application/json'
                },
                body:JSON.stringify(data)
            })

            const content = await response.json()
            if(response.status === 200){
                alert(content.message)
                history.push('/settings')
            }
        }catch(err){
            console.log(err.message)
        }
    }

    const handlePhoneChange = (e) => {
        setMobile(e.target.value)
    }

    return (
        <section className="inmate__form__container">
            <div className="inmate__form__heading">
                {
                    action === 'add' 
                    ?
                    `Add Inmate >`
                    :
                    `Edit Inmate >`
                }
            </div>
            <form autoComplete="off" onSubmit={handleSubmit}>
                <div className="personal__details__section">
                    <div className="inmate__form__secondary__heading flex__container align__items__center">
                        <span>Personal Details</span>
                        <hr/>
                    </div>
                    <div className="inmate__form__body">
                        <div className="flex__container justify__content__between group__container flex__wrap">
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField 
                                    onChange={(e)=>setName(e.target.value)}
                                    id="outlined-basic"
                                    name="name"
                                    type="text"
                                    variant="outlined"
                                    className="input__text__field"
                                    label="Name"
                                    value={name}
                                    required
                                />
                            </FormControl>
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField 
                                    onChange={(e)=>setEmail(e.target.value)}
                                    id="outlined-basic"
                                    name="email"
                                    type="email"
                                    variant="outlined"
                                    className="input__text__field"
                                    label="Email id"
                                    value={email}
                                    required
                                />
                            </FormControl>
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField 
                                    onChange={(e)=>
                                        setMobile(e.target.value)
                                    }
                                    id="outlined-basic"
                                    name="mobile"
                                    variant="outlined"
                                    className="input__text__field"
                                    label="Phone Number"
                                    value={mobile}
                                    required
                                />
                            </FormControl>
                        </div>
                        <label className="dob__label">Date Of Birth :</label>
                        <div className="flex__container justify__content__between group__container align__items__center flex__wrap">
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField 
                                    onChange={(e)=>setDob(e.target.value)}
                                    id="outlined-basic"
                                    name="dob"
                                    type="date"
                                    variant="outlined"
                                    value={dob}
                                    className="input__text__field"
                                    required
                                />
                            </FormControl>
                            <FormControl className="gender__container form__group">
                                <div className="flex__container gender__container">
                                    <label className="form__label" htmlFor="">Gender :</label>
                                    <div className="flex__container align__items__center">
                                        <input
                                            type="radio" 
                                            value="Male" 
                                            id="male" 
                                            name="gender" 
                                            onChange={(e)=>setGender(e.target.value)} 
                                            checked={gender === 'Male'}
                                        />
                                        <label htmlFor="male">Male</label>
                                    </div>

                                    <div className="flex__container align__items__center">
                                        <input 
                                            type="radio" 
                                            value="Female" 
                                            id="female" 
                                            name="gender" 
                                            onChange={(e)=>setGender(e.target.value)}
                                            checked={gender === 'Female'}
                                        />
                                        <label htmlFor="female">Female</label>
                                    </div>

                                    <div className="flex__container align__items__center">
                                        <input 
                                            type="radio"
                                            value="Other" 
                                            id="other" 
                                            name="gender" 
                                            onChange={(e)=>setGender(e.target.value)}
                                            checked={gender === 'Other'}
                                        />
                                        <label htmlFor="other">Other</label>
                                    </div>
                                </div>
                            </FormControl>
                            <FormControl className="form__group">
                            </FormControl>
                        </div>
                    </div>
                </div>
                <div className="other__details__section">
                    <div className="inmate__form__secondary__heading flex__container align__items__center">
                        <span>Other Details</span>
                        <hr/>
                    </div>
                    {
                        action !== 'edit'
                        ?
                        <div className="flex__container justify__content__between group__container flex__wrap">
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField
                                    onChange={(e)=>handleFilterChange(e)}
                                    value={buildingNumber}
                                    name="buildingNumber"
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined Building"
                                    label="Building Number"
                                    select
                                    variant="outlined"
                                    ref={filterRef1}
                                    required
                                >
                                <MenuItem value="" className="select__item">
                                    Select
                                </MenuItem>
                                {
                                    buildings.map((item,index)=>{
                                        return <MenuItem key={index} value={item.id} className="select__item">{item.name}</MenuItem>
                                    })
                                }
                                </CssTextField>
                            </FormControl>
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField
                                    onChange={(e)=>handleFilterChange(e)}
                                    value={wingNumber}
                                    name="wingNumber"
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    label="Wing Number"
                                    select
                                    variant="outlined"
                                    ref={filterRef2}
                                    required
                                >
                                <MenuItem value="" className="select__item">
                                    Select
                                </MenuItem>
                                {
                                    wings.map((item,index)=>{
                                        return <MenuItem key={index} value={item.id} className="select__item">{item.name}</MenuItem>
                                    })
                                }
                                </CssTextField>
                            </FormControl>
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField
                                    onChange={(e)=>handleFilterChange(e)}
                                    value={floorNumber}
                                    name="floorNumber"
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    label="Floor Number"
                                    select
                                    variant="outlined"
                                    ref={filterRef3}
                                    required
                                >
                                <MenuItem value="" className="select__item">
                                    Select
                                </MenuItem>
                                {
                                    floors.map((item,index)=>{
                                        return <MenuItem key={index} value={item.id} className="select__item">{item.name}</MenuItem>
                                    })
                                }
                                </CssTextField>
                            </FormControl>
                        </div>
                            :
                            null 
                        }
                        <div className="flex__container justify__content__between group__container align__items__flex__end flex__wrap">
                        {
                        action !== 'edit'
                        ?
                            <FormControl variant="outlined" className="form__group">
                                <CssTextField
                                    onChange={(e)=>setBedNumber(e.target.value)}
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    label="Bed Number"
                                    value={bedNumber}
                                    select
                                    name="bedNumber"
                                    variant="outlined"
                                    ref={filterRef4}
                                    required
                                >
                                <MenuItem value="" className="select__item">
                                    Select
                                </MenuItem>
                                {
                                    beds.map((item,index)=>{
                                        return <MenuItem key={index} value={item.id} className="select__item">{item.name}</MenuItem>
                                    })
                                }
                                </CssTextField>
                            </FormControl>
                            :
                            null
                        } 
                        <FormControl variant="outlined" className="form__group">
                            <label className="form__label" htmlFor="">Entry date :</label>
                            <CssTextField 
                                onChange={(e)=>setEntryDate(e.target.value)}
                                id="outlined-basic"
                                name="dob"
                                type="date"
                                variant="outlined"
                                value={entryDate}
                                className="input__text__field"
                                required
                            />
                        </FormControl>
                        <FormControl className="form__group"></FormControl>
                    </div>
                </div>
                <div className="inmate__form__footer__buttons">
                {
                    action === 'edit'
                    ?
                    <button type="button" onClick={()=>releaseInmate(inmateId)} className="gray__btn">
                        Release Inmate
                    </button>
                    :
                    null
                } 

                    <Link to="/settings" type="button" className="gray__btn">
                        Cancel
                    </Link>
                    <button type="submit" className="main__blue__btn">
                    {
                        action === 'edit'
                        ?
                        'Save'
                        :
                        'Add'
                    } 
                    </button>
                </div>
            </form>
        </section>
    )
}

export default InmateForm
