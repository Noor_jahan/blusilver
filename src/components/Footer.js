import React from 'react'
import './css/footer.css'

function Footer() {
    return (
        <>
            <div className="margin__fix__footer">
            </div>
            <footer>
                <div className="footer__inner">
                    <h2 className="logo__title">BluSilver</h2>
                </div>
                <span className="footer__stroke"></span>
                <div className="footer__copyright">
                    <span>
                        Copyright (C) 2021 BluGraph Technologies Pvt Ltd | All Rights Reserved
                    </span>
                </div>
            </footer>
        </>
    )
}

export default Footer
