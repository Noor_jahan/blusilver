import React , {useEffect,useRef,useState} from 'react'
import Filter from './Filter'
import './css/alerts.css'
import AlertCard from './AlertCard'
import { formatTimestamp } from '../helpers'
import { useHistory } from 'react-router-dom'

function Alerts() {
    
    const filterRef1 = useRef(null)
    const [loading,setLoading] = useState(true)
    const [filterRefs,setFilterRefs] = useState([])
    const [alerts,setAlerts] = useState([])

    const history = useHistory()
    
    if(localStorage.getItem('login') !== 'true'){
        history.push('/login')
    }

    const filterTypes = [
        {
            genre:'select',
            name:'Timeline',
            ref:filterRef1,
            options:[
                {
                    id:1,
                    name:'1 week',
                }
            ]
        }
    ]

    const clearFilters = ()=>{
        filterRefs.map((item)=>{
            if(item.current){
                item.current.value = ''
            }
            return 0
        })
    }

    const search = ()=>{
        console.log(filterRef1.current.value)
    }

    useEffect(() => {
        
        setFilterRefs([filterRef1])
        let isCancelled = false
        const controller = new AbortController()
        const signal = controller.signal
        const fetchData = async ()=>{
            try{
                let data = {}
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/alert/details', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const content = await response.json();
                if(response.status === 200 && !isCancelled){
                    setAlerts(content.alert_details)
                    setLoading(false)
                }else{
                    alert('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        fetchData()
        return () => {
            isCancelled = true
            console.log("Cancel while unmounting")
            controller.abort()
            setFilterRefs([])
        }
    }, [])

    const handleFilterChange = ()=>{
        console.log("object")
    }

    return (
        <section className="alerts__container">
            <Filter types={filterTypes} handleClearFilter={clearFilters} buttons={true} handleSearch={search} handleFilterChange={handleFilterChange}/>
            <div className="alert__card__container">
                {
                    loading?<div className="loading__container"><div className="loading"></div></div>:null
                }
                { 
                   alerts?.map((item,index)=>{
                        if(item?.message){
                            let type = item?.type === 'on_bed' ? '2' :'1'
                            let date = formatTimestamp(item.timestamp)
                            return <AlertCard key={index} type={type} data={{item}} date={date} item={item}/>
                        }else{
                            return null
                        }
                    })
                }
            </div>
        </section>
    )
}

export default Alerts
