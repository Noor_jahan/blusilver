import React ,{useEffect, useState} from 'react'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar'
import BarChart from './BarChart'
import RectangleSvg from './RectangleSvg'
import TextSvg from './TextSvg'
import './css/chart.css'
import './css/management.css'
import img from '../assets/images/book.svg'
import { useHistory } from 'react-router-dom'

function Management() {
    const [alerts,setAlerts] = useState({normal:50,priority:50})
    const [events,setEvents] = useState({normal:50,priority:50})
    const history = useHistory()

    if(localStorage.getItem('login') !== 'true'){
        history.push('/login')
    }

    const eventOptions = {
        containerWidth:130,
        containerHeight:200,
        containerBackgroundColor:'white',
        bars:[
            {
                barWidth:50,
                barHeight:events?.normal + "%",
                barColor:'#2699fb',
                barText:events?.normal,
                x:0,
            },
            {
                barWidth:50,
                barHeight:events?.priority + "%",
                barColor:'#ffb74a',
                barText:events?.priority,
                x:80,
            }
        ],
        gap:30,
        align:'',
    }

    const alertOptions = {
        containerWidth:130,
        containerHeight:200,
        containerBackgroundColor:'white',
        bars:[
            {
                barWidth:50,
                barHeight:alerts?.normal + "%",
                barColor:'#2699fb',
                barText:alerts?.normal,
                x:0,
            },
            {
                barWidth:50,
                barHeight:alerts?.priority + "%",
                barColor:'#ffb74a',
                barText:alerts?.priority,
                x:80,
            }
        ],
        gap:30,
        align:'',
    }

    useEffect(() => {
        let controller = new AbortController()
        let signal = controller.signal
        const fetchData = async ()=>{
            try{
                let data = {}
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/management_view',{
                    method:'POST',
                    signal:signal,
                    headers:{
                        'Accept':'application/json',
                        'Content-type':'application/json',
                    },
                    body:JSON.stringify(data)
                })

                const content = await response.json()
                setAlerts(content.management_view_details.alerts)
                setEvents(content.management_view_details.events)
            }catch(err){
                console.log(err.message)
            }
        }

        fetchData()
        return () => {
            controller.abort()
            console.log('Cancel while unmounting')
        }
    }, [])

    return (
        <section className="management__container">
            <div className="management__inner__body__container">
                <div className="management__charts__container">
                    <div className="chart__card">
                        <div className="chart__card__header">
                            <hr/>
                            <h3>Occupancy</h3>
                            <hr/>
                        </div>
                        <div className="chart__card__body" >
                            <div className="occupancy__chart__container">
                                <CircularProgressbar value={60} styles={buildStyles({pathColor:'#00ac57',trailColor: '#d6d6d6'})} />
                                <span className="occupancy__inner__content">
                                    <img src={img} alt="book icon"/>
                                    60%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="chart__card">
                        <div className="chart__card__header">
                            <hr/>
                            <h3>Inmates</h3>
                            <hr/>
                        </div>
                        <div className="chart__card__body" >
                            <svg id="inmate__status__chart" preserveAspectRatio="xMidYMax meet" height={170} width="300" style={{backgroundColor:"white"}}>
                                <RectangleSvg x={0} y={0} barWidth={"70%"} barHeight="40" barColor="#2699fb"/>
                                <RectangleSvg x={"70%"} y={0} barWidth={"30%"} barHeight="40" barColor="#ffb74a"/>
                                <TextSvg x={ 0 } fontSize="12" y={60} color={'#555555'} content={"General"} />
                                <TextSvg x={ "70%" } fontSize="12" y={60} color={'#555555'} content={"Priority"} />
                                <TextSvg x={ "3%" } fontSize="20" y={27} color={'white'} content={"70"} />
                                <TextSvg x={ "87%" } fontSize="20" y={27} color={'white'} content={"30"} />

                                <RectangleSvg x={0} y={100} barWidth={"55%"} barHeight="40" barColor="#ffc107"/>
                                <RectangleSvg x={"55%"} y={100} barWidth={"20%"} barHeight="40" barColor="#ff9800"/>
                                <RectangleSvg x={"75%"} y={100} barWidth={"25%"} barHeight="40" barColor="#ff6f00"/>
                                <TextSvg x={ 0 } fontSize="12" y={160} color={'#555555'} content={"Cardio"} />
                                <TextSvg x={ "45%" } fontSize="12" y={160} color={'#555555'} content={"High fall risk"} />
                                <TextSvg x={ "82%" } fontSize="12" y={160} color={'#555555'} content={"memory"} />
                                <TextSvg x={ "3%" } fontSize="20" y={127} color={'white'} content={"55"} />
                                <TextSvg x={ "58%" } fontSize="20" y={127} color={'white'} content={"20"} />
                                <TextSvg x={ "87%" } fontSize="20" y={127} color={'white'} content={"25"} />
                            </svg>
                        </div>
                    </div>
                    <div className="chart__card">
                        <div className="chart__card__header">
                            <hr/>
                            <h3>Events</h3>
                            <hr/>
                        </div>
                        <div className="chart__card__body bars">
                            <span className="bar__title" style={{color:eventOptions.bars[0].barColor}}>Closed</span>
                            <BarChart options={eventOptions}/>
                            <span className="bar__title" style={{color:eventOptions.bars[1].barColor}}>Open</span>
                        </div>
                    </div>
                    <div className="chart__card">
                        <div className="chart__card__header">
                            <hr/>
                            <h3>Alerts</h3>
                            <hr/>
                        </div>
                        <div className="chart__card__body bars">
                            <span className="bar__title" style={{color:eventOptions.bars[0].barColor}}>Normal</span>
                            <BarChart options={alertOptions}/>
                            <span className="bar__title" style={{color:eventOptions.bars[1].barColor}}>Priority</span>
                        </div>
                    </div>
                </div>
                <div className="management__misc__container">
                    <div className="chart__circle__container">
                        <div className="chart__circle__group">
                            <div className="chart__label">
                                Away from bed
                            </div>
                            <div className="outer__circle flex__container align__items__center justify__content__center">
                                0
                            </div>
                        </div>
                        <div className="chart__circle__group">
                            <div className="chart__label">
                                High Temperature
                            </div>
                            <div className="outer__circle flex__container align__items__center justify__content__center">
                                7
                            </div>
                        </div>
                        <div className="chart__circle__group">
                            <div className="chart__label">
                                Fall Risk
                            </div>
                            <div className="outer__circle flex__container align__items__center justify__content__center">
                                3
                            </div>
                        </div>
                        <div className="chart__circle__group">
                            <div className="chart__label">
                                High risk alert
                            </div>
                            <div className="outer__circle flex__container align__items__center justify__content__center">
                                5
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Management
