import React from 'react'
import './css/filter.css'

function Filter({types,handleClearFilter,handleSearch,handleFilterChange,buttons}) {
    
    return (
        <section className="filter__container flex__container flex__wrap">
            {
                types.map((outerItem,index)=>{
                    return (
                            outerItem.genre === 'select' 
                            ?
                            <div className="select__container" key={index}>
                                <label htmlFor={outerItem.name}>{outerItem.name}:</label>
                                <select id={outerItem.name} ref={outerItem.ref} data-type={outerItem.name} onChange={(e)=>handleFilterChange(e)}>
                                    <option value="">Select</option>
                                    {
                                        outerItem.options.map((item,index)=>{
                                            let selected = null
                                            if(outerItem?.defaultValue === item?.id){
                                                selected = "selected"
                                            }
                                            return <option key={index} selected={selected} value={item.id}>{item.name}</option>
                                        })
                                    }
                                </select>
                            </div>
                            :
                            <div className="input__container" key={index}>
                                <input type="text" placeholder={outerItem.placeholder} ref={outerItem.ref} />
                            </div>
                    )
                })
            }
            {
                buttons
                ?
                    <>
                        <button className="main__blue__btn" onClick={handleSearch}>
                            Search
                        </button>
                        <button className="gray__btn" onClick={handleClearFilter}>
                            Clear Filters
                        </button>
                    </>
                :
                null
            }
            
        </section>
    )
}

export default Filter
