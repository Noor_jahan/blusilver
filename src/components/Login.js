import React, { useState } from 'react'
import './css/login.css'
import TextField from '@material-ui/core/TextField'
import {withStyles} from '@material-ui/core/styles'
import InputAdornment from '@material-ui/core/InputAdornment'
import CallIcon from '@material-ui/icons/Call'
import { useHistory } from 'react-router-dom'
import loginImg from  '../assets/images/login.png'
import phoneUtil from 'google-libphonenumber'

const CssTextField = withStyles({
    root: {
        '& .MuiOutlinedInput-root': {
        '& fieldset': {
            borderColor: 'var(--mdcBorderColor)',
            borderWidth: '2px',
        },
        '&:hover fieldset': {
            borderColor: 'var(--mdcBorderColor)',
        },
        '&.Mui-focused fieldset': {
            borderColor: 'var(--mdcBorderColor)',
        },
    },
},
})(TextField);

function Login() {
    const[number,setNumber] = useState('+65 69004397')
    // +65 69004397
    const history = useHistory()

    localStorage.setItem('login',false)

    const handleSubmit = (e)=>{
        e.preventDefault()

        const numberValue = phoneUtil.PhoneNumberUtil.getInstance();
        const phone = numberValue?.parse(number);

        if(!numberValue?.isValidNumber(phone)){
            alert('Please Enter a valid Phone number with Country code');
            return 0
        }
        history.push('/otp-verify')
    }

    return (
        <section className="login__wrapper login__page">
            <div className="login__image__container">
                <img src={loginImg} alt="login image"/>
            </div>
            <div className="login__container">
                <div className="login__logo">
                    <h2 className="logo__title">BluSilver</h2>
                </div>
                <div className="login__body">
                    <div className="login__heading">
                        <h4>Login</h4>
                    </div>
                    <div className="login__form">
                        <form autoComplete="off" onSubmit={handleSubmit}>
                            <CssTextField
                                onChange={(e)=>setNumber(e.target.value)}
                                id="outlined-basic"
                                value={number}
                                variant="outlined"
                                className="input__text__field"
                                placeholder="Enter the Phone Number"
                                InputProps={{
                                    startAdornment: 
                                        <InputAdornment position="start">
                                            <CallIcon style={{"color":"var(--blue)","fontSize":"2rem"}}/>
                                        </InputAdornment>,
                                    }}
                                    required
                            />
                            <button type="submit" className="main__blue__btn" >
                                Get OTP
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login
