import React from 'react'
import logoLoginImg from '../assets/images/logo_new.png'
import logoImg from '../assets/images/logo_new.svg'
import userIcon from '../assets/images/user_icon.svg'
import './css/header.css'
import {Link} from 'react-router-dom'

function Header({addClass,activeNav,name,logo}) {
    let userLoggedIn = localStorage.getItem('login')
    return (
        <header className={addClass}>
            
            <div className="logo__container">
                {
                    addClass
                    ?
                    <img src={logoLoginImg} alt="logo" />
                    :
                    null
                }
                {
                    name
                    ?
                    <img src={logoImg} alt="logo" />
                    :
                    null
                }
                <h2 className="logo__title">{logo}</h2>
            </div>
            <nav className="header__nav">
                {
                    name
                    ?
                        <div className="admin__user__container">
                            <div className="admin__user">
                                <img src={userIcon} alt="user icon"/>
                                <span className="admin__user__name">{name}</span>
                            </div>
                        </div>
                    :
                    null
                }
                {<ul className="nav__list">
                    <li className={`list__item ${activeNav === 'management' ? 'active' : ''}`}>
                        <Link to="/">
                            Home
                        </Link>
                    </li>
                    <li className={`list__item ${activeNav === 'home' ? 'active' : ''}`}>
                        <Link to="/admin">
                            Admin
                        </Link>
                    </li>
                    <li className={`list__item ${activeNav === 'alerts' ? 'active' :''}`}>
                        <Link to="/alerts">
                            Alerts
                        </Link>
                    </li>
                    <li className={`list__item ${activeNav === 'settings' ? 'active' :''}`}>
                        <Link to="/settings">
                            Settings
                        </Link>
                    </li>
                </ul>}
            </nav>
        </header>
    )
}

export default Header
