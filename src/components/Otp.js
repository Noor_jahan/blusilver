import React, { useState, useLayoutEffect, useRef } from 'react'
import { useHistory } from 'react-router'
import './css/otp.css'
import loginImg from  '../assets/images/login.png'

function Otp() {
    const history = useHistory()
    const [otp1,setOtp1] = useState('1')
    const [otp2,setOtp2] = useState('1')
    const [otp3,setOtp3] = useState('1')
    const [otp4,setOtp4] = useState('1')
    const [timeLeft,setTimeLeft] = useState(30)

    const otpRef1 = useRef(null)
    const otpRef2 = useRef(null)
    const otpRef3 = useRef(null)
    const otpRef4 = useRef(null)
    const resetBtnRef = useRef(null)

    const handleOtpSubmit = (e)=>{
        e.preventDefault()
        localStorage.setItem('login',true)
        history.push('/')
    }

    const resendOtpAction = ()=>{
        console.log("object")
    }

    useLayoutEffect(() => {
        const otpRefs = [otpRef1.current , otpRef2.current, otpRef3.current , otpRef4.current]
        otpRef1.current.focus()
        const shiftKeyboardControl = (e,item,index)=>{
            if(e.which === 8 || e.key === 'Backspace' ){
                if(index > 0){
                    otpRefs[index-1].focus()
                }
            }else{
                if(index < 3){
                    otpRefs[index+1].focus()
                }
            }
        }
        otpRefs.map((item,index)=>{
            return item.addEventListener('keyup',(e)=>shiftKeyboardControl(e,item,index))
        })

        let resetTimerIntervalId = setInterval(()=>{
            setTimeLeft((prev)=>{
                if(prev > 1){
                    return prev-1
                }
                resetBtnRef.current.classList.add('active')
                resetBtnRef.current.addEventListener('click',resendOtpAction)
                clearInterval(resetTimerIntervalId)
                return '0'+ 0
            })
        },1000)

        return () => {
            otpRefs.map((item,index)=>{
                return item.removeEventListener('keyup',(e)=>shiftKeyboardControl(e,item,index))
            })
            clearInterval(resetTimerIntervalId)
        }
    }, [])

    return (
        <section className="login__wrapper login__page">
            <div className="login__image__container">
                <img src={loginImg} alt="login image"/>
            </div>
            <div className="login__container otp__container">
                <div className="login__logo">
                    <h2 className="logo__title">BluSilver</h2>
                </div>
                <div className="login__body">
                    <div className="login__heading">
                        <h4>Enter OTP</h4>
                    </div>
                    <div className="login__form otp__form">
                        <form autoComplete="off" onSubmit={handleOtpSubmit}>
                            <div className="input__container">
                                <input type="text" maxLength="1" inputMode="numeric" ref={otpRef1} onChange={(e)=>setOtp1(e.target.value)} required value={otp1}/>
                                <input type="text" maxLength="1" inputMode="numeric" ref={otpRef2} onChange={(e)=>setOtp2(e.target.value)} required value={otp2} />
                                <input type="text" maxLength="1" inputMode="numeric" ref={otpRef3} onChange={(e)=>setOtp3(e.target.value)} required value={otp3}/>
                                <input type="text" maxLength="1" inputMode="numeric" ref={otpRef4} onChange={(e)=>setOtp4(e.target.value)}  required value={otp4}/>
                            </div>   
                            <p className="otp__text">
                                Didn't received OTP?  Resend in 00:{timeLeft}
                            </p>
                            <p className="otp__text resend__btn" ref={resetBtnRef}>
                                Resend
                            </p>
                            <button type="submit" className="main__blue__btn">
                                Submit OTP
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Otp
