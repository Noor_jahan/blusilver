import React from 'react'
import RectangleSvg from './RectangleSvg'
import TextSvg from './TextSvg'

function BarChart({options}){
    var settings = options

    let containerHeight = settings.containerHeight
    let containerWidth = settings.containerWidth
    let containerBackground = settings.containerBackgroundColor
    let barWidth =settings.bars[0].barWidth
    
    let barHeight = settings.bars[0].barHeight
    let barColor = settings.bars[0].barColor
    let barText = settings.bars[0].barText
    let x = 0
    let y = 0

    let gap = settings.gap

    let align = settings.align
    let barX = settings.bars[0].x
    let barY = (parseInt('100%') - parseInt(barHeight)) + '%'
    switch(align){
        case 'top-left':
            x = 0
            y = 0
            break;
        case 'top-right':
            x = containerWidth - barWidth
            y = 0 
            break;
        case 'bottom-left':
            x = 0
            y = containerHeight - barHeight
            break;
        case 'bottom-right':
            x = containerWidth - barWidth
            y = containerHeight - barHeight 
            break;
        case 'top':
            y = 0
            break;
        case 'bottom':
            y = containerHeight - barHeight
            break;
        case 'left':
            x = 0
            break;
        case 'right':
            x = containerWidth - barWidth
            break;
        default:
            x = barX
            y = barY
    }

    let barWidth2 = settings.bars[1].barWidth
    let barHeight2 = settings.bars[1].barHeight
    let barColor2 = settings.bars[1].barColor
    let barText2 = settings.bars[1].barText
    let x2 = settings.bars[1].x
    let y2 = (parseInt('100%') - parseInt(barHeight2) ) + '%'
    return (
        <section>
            <svg style={{'background':containerBackground,'height':containerHeight,'width':containerWidth}} preserveAspectRatio="xMidYMax meet">
                <RectangleSvg x={x} y={y} barWidth={barWidth} barHeight={barHeight} barColor={barColor}/>
                <TextSvg x={ (x+barWidth/2)-10 } fontSize="16" y={(parseInt(y)*containerHeight)/100 + 20} color={'white'} content={barText} />
                {/* <TextSvg x={ 20 } fontSize="16" y={containerHeight-5} color={'blue'} content="General" /> */}
                <RectangleSvg x={x2} y={y2} barWidth={barWidth2} barHeight={barHeight2} barColor={barColor2}/>
                <TextSvg x={ (x2+barWidth2/2)-10 } fontSize="16" y={(parseInt(y2)*containerHeight)/100 + 20} color={'white'} content={barText2} />
                {/* <TextSvg x={ (x2) } fontSize="16" y={containerHeight-5} color={'red'} content="Priority" /> */}
            </svg>            
        </section>
    )
}

export default BarChart