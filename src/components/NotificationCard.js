import React from 'react'
import './css/notificationcard.css'

function NotificationCard({date,title,message,ackClass}) {
    return (
        <div className="notification__card">
            <div className="notification__header flex__container justify__content__between align__items__center">
                <div className={`acknowledge__btn ${ackClass}`}>
                    {ackClass}
                </div>
                <div className="notification__timestamp">
                    {date}
                </div>
            </div>
            <div className="notification__body">
                <h3 className="notification__title">
                    {title}
                </h3>
                <p className="notification__content">
                    {message} 
                </p>
            </div>
        </div>
    )
}

export default NotificationCard
