import React, { useEffect, useRef }  from 'react'

function ChartSample2() {
    const dataBar = {
        labels:['1','2','3'],
        datasets:[
            {
                label:'a',
                data:[10,20,30],
                backgroundColor:'red',
                stack:"0"
            },
            {
                label:'b',
                data:[1,2,3],
                backgroundColor:'green',
                stack:"0"
            },
            {
                label:'c',
                data:[5,10,15],
                backgroundColor:'blue',
                stack:"0"
            },
        ]
    }
    
    const barOptions = {
        reponsive:true,
        datalabels: {
            display: true,
            color: 'white'
        },
        scales:{
            xAxis:{
                stacked:true,
            }
        }
    }

    const chartRef = useRef(null)
    const barRef = useRef(null)
    
    useEffect(()=>{
        Chart.register(ChartDataLabels);
        var myChart = new Chart(barRef.current, {
            type: "bar",
            data: {
              labels: ["Red", "Blue", "Yellow"],
              datasets: [
                {
                  label: "# of Likes",
                  data: [12, 19, 3],
                  backgroundColor: [
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(255, 206, 86, 0.2)"
                  ]
                }
              ]
            }
          });
    },[])
    

    return (
        <div>
             <canvas
          style={{ width: 800, height: 300 }}
          ref={barRef}
        />
            <Bar data={dataBar} options={barOptions} ref={chartRef} />
        </div>
    )
}

export default ChartSample2
