import React, { useEffect , useState } from 'react'
import BedCard from './BedCard'

function Beds({searchData}) {

    const [loading,setLoading] = useState(true)
    const [beds,setBeds] = useState([])

    useEffect(()=>{
        let isCancelled = false
        let controller = new AbortController()
        let signal = controller.signal
        const fetchData = async ()=>{
            setLoading(true)
            try{
                let data = searchData
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/bed/status', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const content = await response.json();

                if(response.status === 200 && !isCancelled){
                    setBeds(content.bed_status)
                    setLoading(false)
                }else{
                    alert('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        fetchData()
        return (()=>{
            isCancelled = true
            controller.abort();
            console.log("Cancel while unmounting")
        })
    },[searchData])

    return (
        <div className="beds__container">
            {
                loading
                ?
                    <div className="loading__container">
                        <div className="loading"></div>
                    </div>
                :
                beds.map((item,index)=>{
                    return <BedCard data={{item}} key={index} />
                })
            }
        </div>
    )
}

export default Beds
