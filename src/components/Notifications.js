import React, { useEffect, useState } from 'react'
import NotificationCard from './NotificationCard'
import { formatTimestamp } from '../helpers'
import {Link} from 'react-router-dom'

function Notifications() {
    const [loading,setLoading] = useState(true);
    const [alerts,setAlerts] = useState([]);

    useEffect(()=>{
        let isCancelled = false
        const controller = new AbortController()
        const signal = controller.signal
        const fetchData = async ()=>{
            try{
                let data = {}
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/alert/details', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const content = await response.json();
                if(response.status === 200 && !isCancelled){
                    setAlerts(content.alert_details)
                    setLoading(false)
                }
            }catch(err){
                console.log(err.message)
            }
        }
        fetchData()
        return (()=>{
            isCancelled = true
            console.log("Cancel while unmounting")
            controller.abort()
        })
    },[])

    return (
        <div className="noifications__container">
            {
                loading
                ?
                    <div className="loading__container">
                        <div className="loading"></div>
                    </div>
                :
                    null
            }
            {
                alerts.map((item,index)=>{
                    if(item.message){
                        let date = formatTimestamp(item.timestamp)
                        let ackClass = item.alert.acknowledge_timestamp !== '' ? 'Acknowledged' : 'Acknowledge'
                        return <NotificationCard key={index} date={date} title={item.inmate_name} message={item.message} ackClass={ackClass} />
                    }else{
                        return null
                    }
                })
            }
            {
                ! loading
                ?
                    <div className="notifications__show__more__btn">
                        <Link to="/alerts" className="show__more__btn">
                            {`View all >`}
                        </Link>
                    </div>
                :
                    null
            }
            
        </div>
    )
}

export default Notifications
