import React ,{useRef,useEffect, useState, useCallback, useLayoutEffect} from 'react'
import Chart from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels'
import './css/chart.css'
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

function ChartSample() {
  const [lineLabels,setLineLabels] = useState(['1'])
  const [lineData,setLineData] = useState([1])

    const dataLine = {
        labels: ['1','2','3','4'],
        datasets: [
        {
            label: '1',
            data: [100,60,70,80],
            fill: false,
            backgroundColor: 'rgb(55, 99, 132)',
            borderColor: 'rgba(255, 99, 132, 0.7)',
        },
        {
            label: '2',
            data: [0,40,30,20],
            fill: false,
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgba(255, 99, 132, 0.7)',
        },
        ],
    };

    const dataLineFill = {
        labels: ['0','1', '2', '3', '4', '5', '6'],
        datasets: [
        {
            label: '2',
            data: [10, 60, 30, 20, 10, 60],
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgba(25, 9, 32, 0.7)',
            fill: true,
        },
        ],
    };

    const dataBar = {
        labels: ['1','2','3','4'],
        datasets:[
            {
                label:'a',
                data: [100,60,70,80],
                backgroundColor:'red',
                stack:"0"
            },
            {
                label:'b',
                data: [0,40,30,20],
                backgroundColor:'green',
                stack:"0"
            },
        ]
    }
    
    const options = {
        scales: {
        yAxes: [
            {
            ticks: {
                beginAtZero: true,
            },
            },
        ],
        plugins: {
            legend: {
                position: 'top',
            },
        }
        },
    };
    
    const barOptions = {
        reponsive:true,
        datalabels: {
            display: true,
            color: 'white'
        },
        scales:{
            xAxis:{
                stacked:true,
            }
        }
    }

    const barRef = useRef(null)
    const barRef2 = useRef(null)
    const areaRef1 = useRef(null)
    
    useEffect(()=>{
      const fetchData = async ()=>{
          try{
            let  data = {}
            const response = await fetch('/sample_chart.json', {
                method: 'GET',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
            });

            const content = await response.json();

            if(response.status === 200){
              // myChart3.data.labels = content.charts.labels
              // console.log(myChart3.data.labels)
              // setLineLabels(content.charts.labels)
              // setLineData(content.charts.data)
              var myChart3 = new Chart(areaRef1.current, {
                    type: "line",
                    data:{
                      labels: content.charts.labels,
                      datasets: [
                        {
                          label: "Breathing rate",
                          data: content.charts.data1,
                          backgroundColor: [
                            "red"
                          ],
                          borderColor:["blue"],
                          fill:false
                        },
                        {
                          label: "Heart rate",
                          data: content.charts.data2,
                          backgroundColor: [
                            "green"
                          ],
                          borderColor:["yellow"],
                          fill:false
                        },
                      ],
                    },
                    options: {
                      scales: {
                          xAxes: [{
                            origin:true
                          }],
                      },
                    }
                  });
            }else{
                alert('Error posting data')
            }

        }catch(err){
            console.log(err.message)
        }
      }

        var myChart = new Chart(barRef.current, {
            plugins: [ChartDataLabels],
            type: "horizontalBar",
            data: {
              labels: ["Red", "Blue", "Yellow"],
              datasets: [
                {
                  label: "# of Likes",
                  data: [12,10,20],
                  backgroundColor: [
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(255, 206, 86, 0.2)"
                  ]
                }
              ]
            },
            options: {
                scales: {
                    xAxes: [{
                        
                      stacked: true,
                      origin:true
                    }],
                    yAxes: [{
                      stacked: true,
                      origin:true
                    }]
                  },
                plugins: {
                  datalabels: {
                    color: 'black',
                    // display: function(context) {
                    //   return context.dataset.data[context.dataIndex] > 15;
                    // },
                    font: {
                      weight: 'bold'
                    },
                    formatter: Math.round
                  }
                },
            }
          });

          var myChart2 = new Chart(barRef2.current, {
            plugins: [ChartDataLabels],
            type: "bar",
            data: {
              labels: ['1','2','3','4'],
          datasets:[
                    {
                        label:'a',
                        data: [100,60,70,80],
                        backgroundColor:'red',
                        stack:"0"
                    },
                    {
                        label:'b',
                        data: [0,40,30,20],
                        backgroundColor:'green',
                        stack:"0"
                    },
                ],
            options: {
                scales: {
                    xAxes: [{
                      stacked: true,
                      origin:true
                    }],
                    yAxes: [{
                      stacked: true
                    }]
                  },
                plugins: {
                  datalabels: {
                    color: 'black',
                    // display: function(context) {
                    //   return context.dataset.data[context.dataIndex] > 15;
                    // },
                    font: {
                      weight: 'bold'
                    },
                    formatter: Math.round
                  }
                },
            }
          }
          });
          
          
      fetchData()

    },[])
    // useLayoutEffect(() => {
    //   console.log(areaRef1)
    //   let a = document.querySelector('#a')
    //   console.log(a)
    //   var myChart3 = new Chart(areaRef1.current, {
    //     type: "line",
    //     data:lineChartData,
    //     options: {
    //       scales: {
    //           xAxes: [{
    //             origin:true
    //           }],
    //       },
    //     }
    //   });
    //   return () => {
    //     cleanup
    //   };
    // }, [input])
    

    return (
        <div>
            {/* <Line data={dataLine} options={options} />
            <Line data={dataLineFill} options={options} />
            <Bar data={dataBar} options={barOptions} /> */}
              <canvas
                style={{ "width": "200 !important", "height": "300 !important" }}
                ref={areaRef1}
              />
        
        <canvas
          style={{ width: 200, height: 300 }}
          ref={barRef}
        />
        <canvas
          style={{ width: 200, height: 300 }}
          ref={barRef2}
        />
        {/* <div className="occupancy__chart__container">
          <div className="occupancy__meter"></div>
        </div> */}
        <div className="occupancy__chart__container">
          <CircularProgressbar value={10} text={`10%`} styles={buildStyles({pathColor:'red',backgroundColor:'green'})} />;
        </div>
        <div className="chart__circle__container">
          <div className="chart__label">
            Away from bed
          </div>
          <div className="outer__circle flex__container align__items__center justify__content__center">
            3
          </div>
          <div className="chart__label">
            High Temperature
          </div>
          <div className="outer__circle flex__container align__items__center justify__content__center">
            30
          </div>
          <div className="chart__label">
            Fall Risk
          </div>
          <div className="outer__circle flex__container align__items__center justify__content__center">
            13
          </div>
        </div>
      </div>
    )
}

export default ChartSample
