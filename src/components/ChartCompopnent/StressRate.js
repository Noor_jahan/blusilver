import React, { useRef, useLayoutEffect,useEffect } from 'react';
import CommonChart from './CommonChart';
import {Link} from 'react-router-dom';
import { useHistory } from 'react-router'

export default function StressRate(props) {

    return (
        <div className="chart__container" >
            <div className="flex__container align__items__center justify__content__between">
            <Link style={{"fontSize":"1.6rem","color":"var(--blue)","marginBottom":"2rem","display":"block"}} to="/alerts">{`<< Back`}</Link>
            <h1>{props?.stateValues?.inmateName}</h1>
            <button className="light__blue__btn" style={{"fontSize":"1.6rem","color":"var(--blue)",visibility:"hidden"}}>{`Next Chart >>`}</button>
            </div>
            <CommonChart chartRate={props?.stateValues?.seriesStressRate} legendName={["Stress Rate"]} legendColor={["#c530309e"]}/>
        </div>
         );
  
}