import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { apiservice } from './Apiservice';
import HeartRate from './HeartRate';
import RestLessNessRate from './RestLessNessRate';
import StressRate from './StressRate';
export default function ChartComponent(){
    const params = useParams()
    const inmateId = params.id
    const inmateName = params.name
    const [vitals, setVitals] = useState();
    const [seriesHeartRate, setSeriesHeartRate] = useState([]);
    const [seriesStressRate, setSeriesStressRate] = useState([]);
    const [seriesRestLessNessRate, setSeriesRestLessNessRate] = useState([]);
    let seriesHeartRate1 =[],seriesStressRate1=[],seriesRestLessNessRate1=[];
    useEffect(()=>{
        let request = {inmate_id:/*inmateId*/"BSB1PW1F1B02I1002",timeline: "1_week"}
        apiservice.fetchVitals(request).then(response => {
            setVitals(response?.data?.vital_details)
            response?.data?.vital_details?.map(eachVital=>{
                seriesHeartRate1.push({date:new Date(eachVital.timestamp),value:eachVital.heart_rate,value2:eachVital.breathing_rate})
                seriesStressRate1.push({date:new Date(eachVital.timestamp),value:eachVital.stress})
                seriesRestLessNessRate1.push({date:new Date(eachVital.timestamp),value:eachVital.restlessness_index})
            })
            setSeriesHeartRate(seriesHeartRate1);setSeriesStressRate(seriesStressRate1);setSeriesRestLessNessRate(seriesRestLessNessRate1)
        })
    },[])
    let pathName = window.location.pathname.split("/")[2];
    let chartName = ""
    let stateValues = {inmateId,inmateName,vitals,seriesHeartRate,seriesStressRate,seriesRestLessNessRate}
    switch(pathName){
        case "heartrate" : chartName=<HeartRate stateValues={stateValues}/>;break;
        case "restlessness" : chartName=<RestLessNessRate stateValues={stateValues}/>;break;
        case "stress" : chartName=<StressRate stateValues={stateValues}/>;break;
        default : break;
    }
    return( <>{chartName}</> )
}