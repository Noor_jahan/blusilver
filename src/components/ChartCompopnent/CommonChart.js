import React, { useRef, useLayoutEffect,useEffect } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);
export default function CommonChart(props) {
  const chart = useRef(null);
  useEffect(() => {
    let x = am4core.create("chartdiv", am4charts.XYChart);
    x.paddingRight = 20;
    let seriesSort = props?.chartRate?.sort(function(a, b){return (new Date(a.date)) - (new Date(b.date));});
    x.data =props?.chartRate
    let dateAxis = x.xAxes.push(new am4charts.DateAxis());
    dateAxis.dataFields.category = "date";
    dateAxis.renderer.grid.template.location = 0;
    dateAxis.renderer.minGridDistance = 30;
    dateAxis.renderer.grid.template.strokeOpacity = 0.1;
    dateAxis.renderer.grid.template.stroke = am4core.color("#A0CA92");
    dateAxis.renderer.grid.template.strokeWidth = 1;

    let valueAxis = x.yAxes.push(new am4charts.ValueAxis());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.minWidth = 35;
    valueAxis.renderer.grid.template.strokeOpacity = 0.1;
    valueAxis.renderer.grid.template.stroke = am4core.color("#A0CA92");
    valueAxis.renderer.grid.template.strokeWidth = 1;
    valueAxis.renderer.ticks.template.length = 6;
    // Create series
    function createSeries(field, name,color) {
      var series = x.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = field;
      series.dataFields.dateX = "date";
      series.name = name;
      series.tooltipText = "[b]{valueY}[/]";
      series.strokeWidth = 2;
      series.stroke = am4core.color(color); // red outline
      series.fill = am4core.color(color);
      series.tensionX = 0.8;
      series.showOnInit = true;
    }
    createSeries("value", props.legendName[0], props.legendColor[0]);
    if(props.legendName.length >1){createSeries("value2",props.legendName[1],props.legendColor[1]);}
    x.legend = new am4charts.Legend();
    x.legend.useDefaultMarker = true;
    x.legend.maxHeight = 150;
    var marker = x.legend.markers.template.children.getIndex(0);
    marker.cornerRadius(12, 12, 12, 12);
    marker.strokeWidth = 0;
    marker.strokeOpacity = 1;

    x.cursor = new am4charts.XYCursor();
    x.cursor.xAxis = dateAxis;
    x.strokeWidth = 2;
    let scrollbarX = new am4charts.XYChartScrollbar();
    x.scrollbarX = scrollbarX;
    chart.current = x;
    return () => { x.dispose();}; 
  },[props.chartRate]);
  return ( <div id="chartdiv" style={{ width: "100%", height: "450px" }}></div> );
}