import axios from "../../axios";

class Apiservices{
    
    fetchVitals(request){
        console.log('request',request)
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
            }
          };
        let url = "https://blusilver-api.blugraph.services/blusilver/vitals";
        let headers = {headers:{'Content-Type':'multipart/form-data' }}
        return axios.post(url,request,axiosConfig)
    }
}

export const apiservice = new Apiservices()