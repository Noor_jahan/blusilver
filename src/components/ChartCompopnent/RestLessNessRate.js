import React, { useRef, useLayoutEffect,useEffect } from 'react';
import CommonChart from './CommonChart';
import {Link} from 'react-router-dom';
import { useHistory } from 'react-router'
export default function RestLessNessRate(props) {
    const history = useHistory()
    const showNextChart = (id,name) =>{history.push(`/chart/stress/${id}/${name}`) }
  return (
    <div className="chart__container" >
        <div className="flex__container align__items__center justify__content__between">
        <Link style={{"fontSize":"1.6rem","color":"var(--blue)","marginBottom":"2rem","display":"block"}} to="/alerts">{`<< Back`}</Link>
        <h1>{props?.stateValues?.inmateName}</h1>
        <button className="light__blue__btn" style={{"fontSize":"1.6rem","color":"var(--blue)"}} onClick={()=>showNextChart(props?.stateValues?.inmateId,props?.stateValues?.inmateName)}>{`Next Chart >>`}</button>
        </div>
         <CommonChart chartRate={props?.stateValues?.seriesRestLessNessRate} legendName={["RestLessNess Rate"]} legendColor={["#4bc0c0cf"]}/>
    </div>
    );
}