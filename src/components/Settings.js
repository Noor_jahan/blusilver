import React, { useRef, useEffect, useState} from 'react'
import Filter from './Filter'
import './css/settings.css'
import PatientsTable from './PatientsTable'
import { Link, useHistory } from 'react-router-dom'
import axios, { cancelToken } from '../axios'

function Settings() {
    const [loading,setLoading] = useState(true)
    const filterRef1 = useRef(null)
    const filterRef2 = useRef(null)
    const filterRef3 = useRef(null)
    const filterRefInput = useRef(null)
    const [filterRefs,setFilterRefs] = useState([])
    const [buildings,setBuildings] = useState([])
    const [floors,setFloors] = useState([])
    const [wings,setWings] = useState([])
    const [rows,setRows] = useState([])

    const history = useHistory()

    if(localStorage.getItem('login') !== 'true'){
        history.push('/login')
    }

    const filterTypes = [
        {
            genre:'input',
            placeholder:'Search inmate',
            ref:filterRefInput
        }
    ]

    const filterTypes2 = [
        {
            genre:'select',
            name:'Building',
            ref:filterRef1,
            options:buildings,
            defaultValue:buildings[0]?.id,
        },
        {
            genre:'select',
            name:'Wing',
            ref:filterRef2,
            options:wings,
            defaultValue:wings[0]?.id,
        },
        {
            genre:'select',
            name:'Floor',
            ref:filterRef3,
            options:floors,
            defaultValue:floors[0]?.id,
        }
    ]

    const clearFilters = ()=>{
        filterRefs.map(async (item)=>{
            if(item.current){
                item.current.value = ''
                try{
                    setLoading(true)
                    let data = {
                        "building_id":filterRef1.current.value,
                        "wing_id":filterRef2.current.value,
                        "floor_id":filterRef3.current.value
                    }
                    const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/details/list/all', {
                        method: 'POST',
                        headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(data)
                    });
                    const content = await response.json();
    
                    if(response.status === 200 ){
                        const array = []
                        let sr_no = 1
                        content.inmate_details.map((item,index)=>{
                            if(item.inmate_name){
                                array.push({...item,sr_no})
                                sr_no++
                            }
                            return null
                        })
                        setRows(array)
                        setLoading(false)
                    }else{
                        console.log('Error while fetching data')
                    }
                }catch(err){
                    console.log(err.message)
                }
            }
            return null
        })
    }

    const search = async ()=>{
        if(filterRefInput.current.value){
            let value = filterRefInput.current.value
            try{
                setLoading(true)
                let data = {
                    "building_id":filterRef1.current.value,
                    "wing_id":filterRef2.current.value,
                    "floor_id":filterRef3.current.value
                }
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/details/list/all', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const content = await response.json();

                if(response.status === 200 ){
                    const array = []
                    let sr_no = 1
                    content.inmate_details.map((item,index)=>{
                        if(item.inmate_name){
                            array.push({...item,sr_no})
                            sr_no++
                        }
                        return null
                    })
                    setRows(array.filter((item)=>item?.inmate_name.includes(value) || item?.email_id.includes(value) || item?.phone_no.includes(value)))
                    setLoading(false)
                }else{
                    console.log('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
    }

    useEffect(() => {
        setFilterRefs([filterRefInput])
        let isCancelled = false
        const controller = new AbortController()
        const signal = controller.source
        let source = cancelToken.source()
        const fetchData = async ()=>{
            try{
                let data = {
                    "building_id":"BSB1001",
                    "wing_id":"BSB1PW1001",
                    "floor_id":"BSB1PW1F1001"
                }
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/details/list/all', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                });
                const content = await response.json();

                if(response.status === 200 && !isCancelled){
                    const array = []
                    let sr_no = 1
                    content.inmate_details.map((item,index)=>{
                        if(item.inmate_name){
                            array.push({...item,sr_no})
                            sr_no++
                        }
                        return null
                    })
                    setRows(array)
                    setLoading(false)
                }else{
                    console.log('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        const fetchData2 = async ()=>{
            try{
                const response = await axios.get('/building_structure',{
                    cancelToken: source.token
                })
                if(response.status === 200 && !isCancelled){
                    const array = []
                    response.data.building_structure.filter((item)=>{
                        return array.push({
                            id:item.building_id,
                            name:item.building_name,
                            wings:item.wing_details,
                        })
                    })
                    setBuildings(array)
                    let array2 = []
                    response.data.building_structure[0].wing_details.map((item)=>{
                        return array2.push({
                            id:item.wing_id,
                            name:item.wing_name,
                            floors:item.floor_details
                        })
                    })
                    setWings(array2)
                    let array3 = []
                    response.data.building_structure[0].wing_details[0].floor_details.map((item)=>{
                        return array3.push({
                            id:item.floor_id,
                            name:item.floor_name,
                        })
                    })
                    setFloors(array3)
                }else{
                    console.log('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        fetchData()
        fetchData2()
        return () => {
            setFilterRefs([])
            isCancelled = true
            controller.abort()
            source.cancel("Cancel while unmounting")
        }
    }, [])

    const handleFilterChange = (e)=>{
        switch(e.target.id){
            case 'Building':
                if(e.target.value !== ''){
                    setWings([])
                    setFloors([])
                    filterRef2.current.value=''
                    filterRef3.current.value=''
                    const buildingSelected = buildings.find((item)=>{
                        return item.id === e.target.value
                    })
                    const wingsInBuilding = buildingSelected.wings
                    const array = []
                    wingsInBuilding.map((item)=>{
                        return array.push({
                            id:item.wing_id,
                            name:item.wing_name,
                            floors:item.floor_details
                        })
                    })
                    setWings(array)
                }
                break;
            case 'Wing':
                if(e.target.value !== ''){
                    setFloors([])
                    const wingSelected = wings.find((item)=>{
                        return item.id === e.target.value
                    })
                    filterRef3.current.value=''
                    const floorsInWing = wingSelected.floors
                    const array = []
                    floorsInWing.map((item)=>{
                        return array.push({
                            id:item.floor_id,
                            name:item.floor_name,
                        })
                    })
                    setFloors(array)
                }else{
                    setFloors([])
                }
                break;
            case 'Floor':
                if(e.target.value !== ''){
                    filterRef3.current.value = e.target.value
                    const fetchData = async ()=>{
                        try{
                            setLoading(true)
                            let data = {
                                building_id:filterRef1.current.value,
                                wing_id:filterRef2.current.value,
                                floor_id:filterRef3.current.value
                            }
                            const response = await fetch('https://blusilver-api.blugraph.services/blusilver/inmate/details/list/all', {
                                method: 'POST',
                                headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                                },
                                body: JSON.stringify(data)
                            });
                            const content = await response.json();
            
                            if(response.status === 200){
                                const array = []
                                let sr_no = 1
                                content.inmate_details.map((item,index)=>{
                                    if(item.inmate_name){
                                        array.push({...item,sr_no})
                                        sr_no++
                                    }
                                    return null
                                })
                                setRows(array)
                                setLoading(false)
                            }else{
                                console.log('Error while fetching data')
                            }
                        }catch(err){
                            console.log(err.message)
                        }
                    }
                    if(filterRef1.current.value !== '' && filterRef2.current.value !== '' && filterRef3.current.value !== '' ){
                        fetchData()
                    }
                }
                break;
            default:
                console.log("Select not found")
        }
    }

    return (
        <section className="settings__container">
            <div className="settings__header flex__container justify__content__between flex__wrap">
                <Filter types={filterTypes} handleClearFilter={clearFilters}  buttons={true} handleSearch={search}/>
                <div className="additional__btns">
                    <Link to="/settings/inmate/create" className="main__blue__btn">
                        <span className="plus__icon">+</span>&nbsp;
                        Add inmate
                    </Link>
                    <Link to="/login" className="gray__btn" onClick={()=>{history.push('/login')}}>
                        Logout
                    </Link>
                </div>
            </div>
            <Filter types={filterTypes2} handleClearFilter={clearFilters} handleSearch={search} buttons={false} handleFilterChange ={handleFilterChange} />
            {
                loading
                ?
                    <div className="loading__container">
                        <div className="loading"></div>
                    </div>
                :
                    <div className="table__wrapper">
                        <PatientsTable rows={rows} />
                    </div>
            }   
        </section>
    )
}

export default Settings
