import React, { useRef, useState, useEffect } from 'react'
import { useParams } from 'react-router'
import Chart from 'chart.js'
import { formatTimestamp, formatTimestampForCharts } from '../helpers'
import './css/chart.css'
import { Link } from 'react-router-dom'

function RestlessnessChart(){
    const params = useParams()
    const inmateId = params.id
    const inmateName = params.name

    const chartRef = useRef(null)

    const[labels,setLabels] = useState([])
    const[breathingRate,setBreathingRates] = useState([])
    const[heartRate,setHeartRates] = useState([])
    const[restlessness,setRestlessness] = useState([])
    const[stress,setStress] = useState([])
    const[chartDatasets,setChartDatasets] = useState([])
    const[isLoading,setIsLoading] = useState(true)
    const[currentChart,setCurrentChart] = useState('restlessness')

    useEffect(() => {
        let controller = new AbortController()
        let signal = controller.signal
        const fetchData = async ()=>{
            try{
                let  data = {
                    inmate_id:inmateId,
                    timeline:"1_week"
                }
                const response = await fetch('https://blusilver-api.blugraph.services/blusilver/vitals', {
                    method: 'POST',
                    signal:signal,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(data)
                });

                const content = await response.json();

                if(response.status === 200){
                    if(content.vital_details.length === 0){
                        alert('No data available')
                        return false
                    }
                    let heart_rate_array = []
                    let breathing_rate_array = []
                    let restlessness_array = []
                    let timestamp_array = []
                    // let timestamp_set = []
                    let stress_array = []
                    content.vital_details.slice(0,200).map((item)=>{
                        breathing_rate_array.push(item.breathing_rate)
                        heart_rate_array.push(item.heart_rate)
                        restlessness_array.push(item.restlessness_index)
                        stress_array.push(item.stress)
                        // timestamp_array.push(formatTimestampForCharts(item.timestamp))
                        if(timestamp_array.includes(formatTimestampForCharts(item.timestamp))){
                            timestamp_array.push('')
                        }else{
                            timestamp_array.push(formatTimestampForCharts(item.timestamp))
                        }
                        return 0
                    })

                    setLabels(timestamp_array)
                    // console.log(timestamp_set)
                    // setLabels(timestamp_set)
                    setHeartRates(heart_rate_array)
                    setBreathingRates(breathing_rate_array)
                    setRestlessness(restlessness_array)
                    setStress(stress_array)

                    setChartDatasets([
                        {
                            type:'line',
                            label: "Restlessness Index",
                            data: restlessness_array,
                            backgroundColor: [
                                "rgba(75, 192, 192, 0.2)"
                            ],
                            borderColor:["rgba(75, 192, 192, 1)"],
                            // pointBorderColor:'rgba(255, 99, 132, 1)',
                            // pointBackgroundColor:'rgba(255, 99, 132, 1)',
                            fill:true,
                        }
                    ],)

                    setIsLoading(false)
                    }else{
                        alert('Error posting data')
                    }
            }catch(err){
                console.log(err.message)
            }
        }
        if(chartDatasets.length === 0){
            fetchData()
        }else{
            setIsLoading(false)
        }
        var myChart3 = new Chart(chartRef.current, {
            type: "line",
            data:{
                labels: labels,
                datasets: chartDatasets
            },
            options: {
                legend: {
                    labels: {
                        fontSize: window.innerWidth > 550 ? 20 : 10
                    }
                },
                spanGaps:true,
                scales: {
                    xAxes: [{
                        origin:true,
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                },
            }
        });
        return () => {
            console.log("Unmounting")
            controller.abort()
        }
    }, [isLoading])

    const showNextChart = (chart)=>{
        setIsLoading(true)
        if(chart === 'heart'){
            setCurrentChart('restlessness')
            setChartDatasets([
                {
                    type:'line',
                    label: "Restlessness Index",
                    data: restlessness,
                    backgroundColor: [
                        "rgba(75, 192, 192, 0.2)"
                    ],
                    borderColor:["rgba(75, 192, 192, 1)"],
                    // pointBorderColor:'rgba(255, 99, 132, 1)',
                    // pointBackgroundColor:'rgba(255, 99, 132, 1)',
                    fill:true,
                }
            ],)
        }else if(chart ==='restlessness'){
            setCurrentChart('heart')
            setChartDatasets([
                {
                    label: "Breathing rate",
                    data: breathingRate,
                    backgroundColor: [
                        "rgba(197,48,48,0.2)"
                    ],
                    borderColor:["rgba(255, 99, 132, 1)"],
                    fill:false
                },
                {
                    label: "Heart rate",
                    data: heartRate,
                    backgroundColor: [
                        "rgba(75, 192, 192, 0.2)"
                    ],
                    borderColor:["rgba(75, 192, 192, 1)"],
                    fill:false
                },
            ],)
        }
        // else if(chart ==='stress'){
        //     setCurrentChart('heart')
        //     setChartDatasets([
        //         {
        //             label: "Breathing rate",
        //             data: breathingRate,
        //             backgroundColor: [
        //                 "rgba(197,48,48,0.2)"
        //             ],
        //             borderColor:["rgba(255, 99, 132, 1)"],
        //             fill:false
        //         },
        //         {
        //             label: "Heart rate",
        //             data: heartRate,
        //             backgroundColor: [
        //                 "rgba(75, 192, 192, 0.2)"
        //             ],
        //             borderColor:["rgba(75, 192, 192, 1)"],
        //             fill:false
        //         },
        //     ],)
        // }
    }

    return (
        <div className="chart__container" >
            <Link style={{"fontSize":"1.6rem","color":"var(--blue)","marginBottom":"2rem","display":"block"}} to="/alerts">{`< Back`}</Link>
            {
                isLoading
                ?
                    <div className="loading__chart__container">
                        <span>Loading...</span>
                        <div className="loading"></div>
                    </div>
                :
                    <div className="flex__container align__items__center justify__content__between">
                        <h1>{inmateName}</h1>
                        <button className="light__blue__btn" style={{"fontSize":"1.6rem","color":"var(--blue)"}} onClick={()=>showNextChart(currentChart)}>{`Next Chart >`}</button>
                    </div>
            }
            <canvas style={{"paddingBottom":"10rem"}}
                ref={chartRef}
            />
        </div>
    )
}

export default RestlessnessChart