import React from 'react'
import blueBed from '../assets/images/bed_blue_new.svg'
import grayBed from '../assets/images/bed_gray.svg'
import redBed from '../assets/images/bed_red_new.svg'
import './css/bedcard.css'

function BedCard({data}) {
    const {item} = data
    return (
        <div className="bed__card">
            <div className="bed__card__header flex__container flex__wrap align__items__center">
                <div className="light__blue__btn">
                    <span className="bed__card__header__title">
                        Building :
                    </span>
                    <span className="bed__card__header__value">
                        {item.building_name}
                    </span>
                </div>
                <div className="light__blue__btn">
                    <span className="bed__card__header__title">
                        Wing :
                    </span>
                    <span className="bed__card__header__value">
                        {item.wing_no}
                    </span>
                </div>
                <div className="light__blue__btn">
                    <span className="bed__card__header__title">
                        Floor :
                    </span>
                    <span className="bed__card__header__value">
                        {item.floor_no}
                    </span>
                </div>
            </div>
            <div className="bed__card__body flex__container flex__wrap">
                {
                    item.beds.map((i,index)=>{
                        let bedNumber = i.bed_id.slice(-2)
                        if(i.occupancy_status === "assigned"){
                            let bedType, bedClass
                            if(i.alert_status === 0){
                                bedType = blueBed 
                                bedClass = "light__blue__bed"
                            }else{
                                bedType = redBed 
                                bedClass = "light__red__bed"
                            }
                            return (
                                <div className={bedClass} key={index}>
                                    <img src={bedType} alt="all good bed" />
                                    <span className="bed__number">{bedNumber}</span>
                                </div>
                            )
                        }else{
                            return (
                                <div className="light__blue__bed gray" key={index}>
                                    <img src={grayBed} alt="all good bed" />
                                    <span className="bed__number">{bedNumber}</span>
                                </div>
                            )
                        }
                    })
                }
                {/* <div className="light__blue__bed">
                    <img src={blueBed} alt="all good bed" />
                    <span className="bed__number">1</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueDoor} alt="all good door" />
                    <span className="bed__number">2</span>
                </div>
                <div className="light__red__bed">
                    <img src={redBed} alt="alert bed" />
                    <span className="bed__number">3</span>
                </div>
                <div className="light__red__bed">
                    <img src={redDoor} alt="alert door" />
                    <span className="bed__number">4</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueBed} alt="all good bed" />
                    <span className="bed__number">1</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueDoor} alt="all good door" />
                    <span className="bed__number">2</span>
                </div>
                <div className="light__red__bed">
                    <img src={redBed} alt="alert bed" />
                    <span className="bed__number">3</span>
                </div>
                <div className="light__red__bed">
                    <img src={redDoor} alt="alert door" />
                    <span className="bed__number">4</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueBed} alt="all good bed" />
                    <span className="bed__number">1</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueDoor} alt="all good door" />
                    <span className="bed__number">2</span>
                </div>
                <div className="light__red__bed">
                    <img src={redBed} alt="alert bed" />
                    <span className="bed__number">3</span>
                </div>
                <div className="light__red__bed">
                    <img src={redDoor} alt="alert door" />
                    <span className="bed__number">4</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueBed} alt="all good bed" />
                    <span className="bed__number">1</span>
                </div>
                <div className="light__blue__bed">
                    <img src={blueDoor} alt="all good door" />
                    <span className="bed__number">2</span>
                </div>
                <div className="light__red__bed">
                    <img src={redBed} alt="alert bed" />
                    <span className="bed__number">3</span>
                </div>
                <div className="light__red__bed">
                    <img src={redDoor} alt="alert door" />
                    <span className="bed__number">4</span>
                </div> */}
            </div>
        </div>
    )
}

export default BedCard
