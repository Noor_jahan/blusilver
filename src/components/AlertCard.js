import React from 'react'
import { useHistory } from 'react-router'
import './css/alertcard.css'

function AlertCard({data,date,type}){  
    const {item} = data
    const history = useHistory()
    const ackClass = 
        item?.alert?.acknowledge_timestamp !== ''
        ?
        'Acknowledged'
        :
        'Acknowledge'

    const showHeartChart = (id,name)=>{
        history.push(`/chart/heartrate/${id}/${name}`)    
    }

    const showRestlessnessChart = (id,name)=>{
        history.push(`/chart/restlessness/${id}/${name}`)    
    }

    const showStressChart = (id,name)=>{
        history.push(`/chart/stress/${id}/${name}`)    
    }

    return (
        <div className="alert__card flex__container">
            <div className="alert__left__container">
                <div className="notification__header align__items__center justify__content__between flex__container">
                    <button className={`acknowledge__btn ${ackClass}`}>
                        {ackClass}
                    </button>
                    <p>
                        <span className="bold__text">Satus : </span>
                        {
                            type === "2" 
                            ?
                            <span className="blue__text bold__text">
                                ON-BED
                            </span>
                            :
                            <span className="red__text bold__text">
                                OFF-BED
                            </span>
                        }
                    </p>
                </div>
                <div className="notification__body">
                    <h3 className="notification__title">
                        {item?.inmate_name}
                    </h3>
                    <p className="notification__content">
                        {item?.message}
                    </p>
                </div>
            </div>
            <div className="alert__middle__container">
                {/* <p className="alert__middle__row">
                    <span className="bold__text">
                        Type :
                    </span>
                    
                </p> */}
                <p className="alert__middle__row">
                    <span className="bold__text" className="bold__text">
                        Building :
                    </span>
                    <span className="light__text">
                        {item?.building_name}
                    </span>
                </p>
                <p className="alert__middle__row">
                    <span className="bold__text" className="bold__text">
                        Wing :
                    </span>
                    <span className="light__text">
                        {item?.wing_no}
                    </span>
                </p>
                <p className="alert__middle__row">
                    <span className="bold__text">
                        Floor :
                    </span>
                    <span className="light__text">
                        {item?.floor_no}
                    </span>
                </p>
                <p className="alert__middle__row">
                    <span className="bold__text">
                        Bed number :
                    </span>
                    <span className="light__text">
                        {item?.bed_no}
                    </span>
                </p>
            </div>
            <div className="alert__right__container">
                <p>
                    <span className="bold__text">
                        Alert time  :
                    </span>
                    <span className="light__text">
                        {date}
                    </span>
                </p>
                <p>
                    <span className="bold__text">
                        Acknowledgement time  :
                    </span>
                    <span className="light__text">
                        {
                            item?.alert?.acknowledge_timestamp !== ''
                            ?
                            item?.alert?.acknowledge_timestamp
                            :
                            'Not Acknowledged'
                        }
                    </span>
                </p>
                {
                    type === "2" 
                    ?
                        <div className="parameters__container">
                            {
                                item?.alert?.heart_rate === 'normal' 
                                ?
                                    <button onClick={()=>showHeartChart(item?.inmate_id,item?.inmate_name)} className="light__blue__btn">
                                        <span className="light__text">
                                            Heart rate :
                                        </span>
                                        <span className="blue__text">
                                            {parseFloat(item?.heart_rate).toFixed(2)}
                                        </span>
                                    </button>
                                :
                                    <button onClick={()=>showHeartChart(item?.inmate_id,item?.inmate_name)} className="light__red__btn">
                                        <span className="red__text">
                                            Heart rate :
                                        </span>
                                        <span className="red__text">
                                            {parseFloat(item?.heart_rate).toFixed(2)}
                                        </span>
                                    </button>
                            }
                            {
                                item?.alert?.stress_level === 'normal' 
                                ?
                                    <button className="light__blue__btn" onClick={()=>showStressChart(item?.inmate_id,item?.inmate_name)}>
                                        <span className="light__text">
                                            Stress Level :
                                        </span>
                                        <span className="blue__text">
                                            {parseFloat(item?.stress).toFixed(2)}
                                        </span>
                                    </button>
                                :
                                    <button className="light__red__btn" onClick={()=>showStressChart(item?.inmate_id,item?.inmate_name)}>
                                        <span className="red__text">
                                            Stress Level :
                                        </span>
                                        <span className="red__text">
                                            {parseFloat(item?.stress).toFixed(2)}
                                        </span>
                                    </button>
                            }
                            {
                                item?.alert?.restlessnes_index === 'normal' 
                                ?
                                    <button onClick={()=>showRestlessnessChart(item?.inmate_id,item?.inmate_name)} className="light__blue__btn">
                                        <span className="light__text">
                                            Restlessness :
                                        </span>
                                        <span className="blue__text">
                                            {parseFloat(item?.restlessnes_index).toFixed(2)}
                                        </span>
                                    </button>
                                :
                                    <button onClick={()=>showRestlessnessChart(item?.inmate_id,item?.inmate_name)} className="light__red__btn">
                                        <span className="red__text">
                                            Restlessness :
                                        </span>
                                        <span className="red__text">
                                            {parseFloat(item?.restlessnes_index).toFixed(2)}
                                        </span>
                                    </button>
                            }
                            {/* <button className="light__blue__btn">
                                <span className="light__text">
                                    Restlessness :
                                </span>
                                <span className="blue__text">
                                    Yes
                                </span>
                            </button> */}
                            {
                                item?.alert?.breathing_rate === 'normal' 
                                ?
                                    <button onClick={()=>showHeartChart(item?.inmate_id,item?.inmate_name)} className="light__blue__btn">
                                        <span className="light__text">
                                            Breathing rate :
                                        </span>
                                        <span className="blue__text">
                                            {parseFloat(item?.breathing_rate).toFixed(2)}
                                        </span>
                                    </button>
                                :
                                    <button onClick={()=>showHeartChart(item?.inmate_id,item?.inmate_name)} className="light__red__btn">
                                        <span className="red__text">
                                            Breathing rate :
                                        </span>
                                        <span className="red__text">
                                            {parseFloat(item?.breathing_rate).toFixed(2)}
                                        </span>
                                    </button>
                            }
                        </div>
                    :
                        <p>
                            <span className="bold__text">
                                Reason  :
                            </span>
                            <span className="light__text">
                                {item?.reason ? item?.reason : 'OFF-BED'}
                            </span>
                        </p>
                }
            </div>
        </div>
    )
}
export default AlertCard
