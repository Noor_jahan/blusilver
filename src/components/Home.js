import React , { useRef, useState, useEffect } from 'react'
import './css/home.css'
import Filter from './Filter'
import Beds from './Beds'
import Notifications from './Notifications'
import './css/home.css'
import axios, {cancelToken} from '../axios'
import { useHistory } from 'react-router-dom'

function Home() {
    const filterRef1 = useRef(null)
    const filterRef2 = useRef(null)
    const filterRef3 = useRef(null)

    const history = useHistory()
    
    if(localStorage.getItem('login') !== 'true'){
        history.push('/login')
    }

    const [filterRefs,setFilterRefs] = useState([])
    const [buildings,setBuildings] = useState([])
    const [floors,setFloors] = useState([])
    const [wings,setWings] = useState([])

    const [searchData,setSearchData] = useState({})
    
    const filterTypes = [
        {
            genre:'select',
            name:'Building',
            ref:filterRef1,
            options:buildings
        },
        {
            genre:'select',
            name:'Wing',
            ref:filterRef2,
            options:wings
        },
        {
            genre:'select',
            name:'Floor',
            ref:filterRef3,
            options:floors
        }
    ]

    const clearFilters = ()=>{
        setWings([])
        setFloors([])
        let data = {}
        setSearchData(data)
        filterRefs.map((item)=>{
            if(item.current){
                item.current.value = ''
            }
            return 0
        })

    }

    const search = ()=>{
        let building_id = (filterRef1.current.value)
        let wing_id = (filterRef2.current.value)
        let floor_id = (filterRef3.current.value)
        if(building_id !== '' && wing_id !== '' && floor_id !== ''){
            let data = {
                building_id,
                wing_id,
                floor_id
            }
            setSearchData(data)
        }
    }

    useEffect(() => {
        setFilterRefs([filterRef1,filterRef2,filterRef3])
        let isCancelled = false
        let source = cancelToken.source()
        const fetchData = async ()=>{
            try{
                    const response = await axios.get('/building_structure',{
                    cancelToken: source.token
                })
                if(response.status === 200 && !isCancelled){
                    const array = []
                    response.data.building_structure.filter((item)=>{
                        return array.push({
                            id:item.building_id,
                            name:item.building_name,
                            wings:item.wing_details,
                        })
                    })
                    setBuildings(array)
                }else{
                    alert('Error while fetching data')
                }
            }catch(err){
                console.log(err.message)
            }
        }
        fetchData()
        return () => {
            isCancelled = true
            source.cancel('Cancel while unmounting')
            setFilterRefs([])
        }
    }, [])

    const handleFilterChange = (e)=>{
        switch(e.target.id){
            case 'Building':
                if(e.target.value !== ''){
                    setWings([])
                    setFloors([])
                    filterRef2.current.value=''
                    filterRef3.current.value=''
                    const buildingSelected = buildings.find((item)=>{
                        return item.id === e.target.value
                    })
                    const wingsInBuilding = buildingSelected.wings
                    const array = []
                    wingsInBuilding.map((item)=>{
                        return array.push({
                            id:item.wing_id,
                            name:item.wing_name,
                            floors:item.floor_details
                        })
                    })
                    setWings(array)
                }
                break;
            case 'Wing':
                if(e.target.value !== ''){
                    setFloors([])
                    const wingSelected = wings.find((item)=>{
                        return item.id === e.target.value
                    })
                    filterRef3.current.value=''
                    const floorsInWing = wingSelected.floors
                    const array = []
                    floorsInWing.map((item)=>{
                        return array.push({
                            id:item.floor_id,
                            name:item.floor_name,
                        })
                    })
                    setFloors(array)
                }
                break;
            default:
                console.log("Select not found")
        }
    }

    return (
        <section className="home__container">
            <Filter types={filterTypes} handleClearFilter={clearFilters} handleSearch={search} handleFilterChange={handleFilterChange} buttons={true} />
            <div className="flex__container home__body justify__content__between">
                <Beds searchData={searchData} />
                <Notifications />
            </div>
        </section>
    )
}

export default Home
