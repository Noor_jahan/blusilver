import React from 'react'

function TextSvg({x,fontSize,y,color,content}) {
    return (
        <text id="text__svg" x={ x } width={'20px'} fontSize={fontSize} y={y} fill={color}>{content}</text>   
    )
}

export default TextSvg
